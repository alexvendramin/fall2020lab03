package LinearAlgebra;

public class Vector3d {

	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		double x = this.x;
		double y = this.y;
		double z = this.z;
		x = x * x;
		y = y * y;
		z = z * z;
		double magnitude = x + y + z;
		magnitude = Math.sqrt(magnitude);
		return magnitude;
	}
	
	public double dotProduct(Vector3d vector) {
		double x1 = this.x;
		double y1 = this.y;
		double z1= this.z;
		double x2 = vector.getX();
		double y2 = vector.getY();
		double z2 = vector.getZ();
		x1 = x1 * x2;
		y1 = y1 * y2;
		z1 = z1 * z2;
		double product = x1 + y1 + z1;
		return product;
	}
	
	public Vector3d add(Vector3d vector) {
		double x1 = this.x;
		double y1 = this.y;
		double z1 = this.z;
		double x2 = vector.getX();
		double y2 = vector.getY();
		double z2 = vector.getZ();
		x1 = x1 + x2;
		y1 = y1 + y2;
		z1 = z1 + z2;
		Vector3d temp = new Vector3d(x1, y1, z1);
		return temp;
	}
}
