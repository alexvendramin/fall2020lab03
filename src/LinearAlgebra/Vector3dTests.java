package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGets() {
		Vector3d vector = new Vector3d(1.0,2.0,3.0);
		assertEquals(vector.getX(), 1.0);
		assertEquals(vector.getY(), 2.0);
		assertEquals(vector.getZ(), 3.0);
	}

	@Test
	void testMagnitude() {
		Vector3d vector = new Vector3d(1.0,2.0,2.0);
		assertEquals(vector.magnitude(), 3.0);
	}
	
	@Test
	void testDotProduct(){
		Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
		Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
		assertEquals(vector1.dotProduct(vector2), 13.0);
		
	}
	
	@Test
	void testAdd() {
		Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
		Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
		Vector3d vector3 = new Vector3d(3.0, 4.0, 6.0);
		assertEquals(vector1.add(vector2).getX(), vector3.getX());
		assertEquals(vector1.add(vector2).getY(), vector3.getY());
		assertEquals(vector1.add(vector2).getZ(), vector3.getZ());
	}
}
